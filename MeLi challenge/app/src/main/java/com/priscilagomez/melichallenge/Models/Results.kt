package com.priscilagomez.melichallenge.Models

import java.io.Serializable


data class Results(
                val id : String,
                val site_id : String,
                val subtitle : String,
                val seller_id : Int,
                val category_id : String,
                val official_store_id : Int,
                val price : Double,
                val base_price : Double,
                val original_price : Double,
                val currency_id : String,
                val initial_quantity : Int,
                val buying_mode : String,
                val condition : String,
                val permalink : String,
                val thumbnail_id : String,
                val thumbnail : String,
                val secure_thumbnail : String,
                val accepts_mercadopago : Boolean,
                val installments : Installments

) : Serializable
