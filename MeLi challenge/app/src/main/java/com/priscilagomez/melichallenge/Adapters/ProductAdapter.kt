package com.priscilagomez.melichallenge.Adapters

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.priscilagomez.melichallenge.Activities.ProductDetailActivity
import com.priscilagomez.melichallenge.Models.Results
import com.priscilagomez.melichallenge.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_product_detail.*
import kotlinx.android.synthetic.main.product_row.view.*


class ProductAdapter(private val products: List<Results>) : RecyclerView.Adapter<ProductAdapter.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(item: Results) {
           itemView.txtProductName.text = item.subtitle
            itemView.txtProductPrice.text = itemView.context.getString(R.string.symbol) + item.price.toString()
            Picasso.get()
                    .load(item.thumbnail)
                    .resize(250, 250)
                    .centerCrop()
                    .into(itemView.imgProduct)

            val installmentsValues = item.installments.amount / item.installments.quantity
            itemView.txtInstallments.text = itemView.context.getString(R.string.up_to) + item.installments.quantity.toString() + itemView.context.getString(R.string.installments) + itemView.context.getString(R.string.symbol) + String.format("%.2f", installmentsValues)

            itemView.setOnClickListener(View.OnClickListener {
                val intent = Intent(itemView.context, ProductDetailActivity::class.java)
                intent.putExtra("data", item)
                itemView.context.startActivity(intent)

            })
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.product_row, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = products[position]
        holder.bind(item)
    }

    override fun getItemCount(): Int {
        return products.size
    }
}