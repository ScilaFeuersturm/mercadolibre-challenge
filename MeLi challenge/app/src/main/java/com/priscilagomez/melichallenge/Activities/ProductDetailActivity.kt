package com.priscilagomez.melichallenge.Activities

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.priscilagomez.melichallenge.Models.Results
import com.priscilagomez.melichallenge.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_product_detail.*
import java.io.Serializable
import java.lang.StringBuilder


class ProductDetailActivity : AppCompatActivity(), Serializable{
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_detail)
        val toolbar: Toolbar = findViewById<View>(R.id.toolbar_detail) as Toolbar
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setIcon(R.mipmap.ic_launcher)
        supportActionBar?.setDisplayShowTitleEnabled(false)


        val product = intent.getSerializableExtra("data") as? Results


        if (product != null) {
            Picasso.get()
                    .load(product.thumbnail)
                    .resize(400, 400)
                    .centerCrop()
                    .into(imgProductDetail)

            txtpriceProduct.text = product.price.toString()

            val sb= StringBuilder()
            sb.append(getString(R.string.condition))

            if (product.condition.contains("new")){
                sb.append(getString(R.string.condition_new))

            }else{
                sb.append(getString(R.string.condition_used))
            }
            txtCondition.text = sb.toString()

            val installments = product.installments
            val installmentsValues = installments.amount / installments.quantity
            val stringBuilder = StringBuilder()
            stringBuilder.append(getString(R.string.or))
            stringBuilder.append(installments.quantity.toString())
            stringBuilder.append(getString(R.string.installments))
            stringBuilder.append(getString(R.string.symbol))
            stringBuilder.append(String.format("%.2f", installmentsValues))
            txtinstallmentsProduct.text = stringBuilder.toString()
        }

    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}