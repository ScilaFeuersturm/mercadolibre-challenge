package com.priscilagomez.melichallenge.Models

data class MLResponse(val siteId : String, val query : String, val paging : Paging, val results : ArrayList<Results>)

