package com.priscilagomez.melichallenge.Models

data class Paging (val total : Int, val primary_results: Int, val offset : Int, val limit : Int )

