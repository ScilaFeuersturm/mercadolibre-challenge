package com.priscilagomez.melichallenge.Activities

import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import com.priscilagomez.melichallenge.Adapters.ProductAdapter
import com.priscilagomez.melichallenge.Models.MLResponse
import com.priscilagomez.melichallenge.Models.Results
import com.priscilagomez.melichallenge.R
import com.priscilagomez.melichallenge.Retrofit.APIService
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : AppCompatActivity(), SearchView.OnQueryTextListener {

    lateinit var productAdapter: ProductAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        val toolbar = findViewById<Toolbar>(R.id.toolbar_main)
        setSupportActionBar(toolbar)
        supportActionBar?.setIcon(R.mipmap.ic_launcher)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        searchProducts.setOnQueryTextListener(this)

    }


    private fun getUsers(query: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl("https://api.mercadolibre.com/sites/MLA/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val apiService = retrofit.create(APIService::class.java)
        apiService.getProductInfo("search?q=$query").enqueue(object : Callback<MLResponse> {
            override fun onResponse(call: Call<MLResponse>, response: Response<MLResponse>) {
                if (response.isSuccessful) {
                    var responseSuccess = response.body()
                    var results = responseSuccess!!.results
                    if(results.size > 0){
                        initProduct(results)
                    }else{
                        customAlert(getString(R.string.information), getString(R.string.no_results))
                    }
                }
            }

            override fun onFailure(call: Call<MLResponse>, t: Throwable) {
                customAlert(getString(R.string.information), getString(R.string.error_message))
            }
        })
    }

    private fun initProduct(products: List<Results>) {
        productAdapter = ProductAdapter(products)
        rvProduct.setHasFixedSize(true)
        rvProduct.layoutManager = LinearLayoutManager(this)
        rvProduct.adapter = productAdapter
    }


    private fun customAlert(title : String, message : String ) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(title)
        builder.setMessage(message)

        builder.setPositiveButton(R.string.Ok) { dialog, _ ->
            dialog.dismiss()
        }
        builder.show()
    }

    override fun onQueryTextSubmit(query: String): Boolean {
        var modifiedQuery = query.replace("\\s".toRegex(), "%20")
        getUsers(modifiedQuery.toLowerCase())
        return true
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        return true
    }

}




