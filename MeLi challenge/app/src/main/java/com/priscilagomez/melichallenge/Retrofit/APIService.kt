package com.priscilagomez.melichallenge.Retrofit

import com.priscilagomez.melichallenge.Models.MLResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Url

interface APIService {
    @GET
    fun getProductInfo(@Url url:String): Call<MLResponse>
}