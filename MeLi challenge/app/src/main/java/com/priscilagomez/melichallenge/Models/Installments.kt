package com.priscilagomez.melichallenge.Models

import java.io.Serializable

data class Installments (
    val quantity : Int,
    val amount: Double,
    val rate: Double,
    val currency_id : String
    ) : Serializable
